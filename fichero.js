function ClasificarJuguetes(diametro) {

    if (diametro <= 10) {
        console.log("Es una rueda para un juguete pequeño.");
    }

    if (diametro > 10 && diametro < 20) {
        console.log("Es una rueda para un juguete mediano.");
    }
    if (diametro >= 20) {
        console.log("Es una rueda para un juguete grande.");
    }
}

ClasificarJuguetes(20);
